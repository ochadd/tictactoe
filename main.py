import sys


class InvalidMove(Exception):
    def __init__(self, message):
        super().__init__(message)


class WinException(Exception):
    def __init__(self, message):
        super().__init__(message)


class TiedException(Exception):
    def __init__(self, message):
        super().__init__(message)


class TicTacToe(object):
    empty_space = '-'
    player_value = 'x'
    computer_value = 'o'
    columns = {
        'a': 0,
        'b': 1,
        'c': 2,
    }

    def __init__(self, board_size):
        self.board_size = board_size
        self.board = self.make_board(board_size)

    @classmethod
    def make_board(cls, size):
        return [[cls.empty_space for _ in range(size)] for _ in range(size)]

    def print_board(self):
        for row in self.board:
            print('|'.join(row))

    def validate_move(self, move):
        self.validate_move_length(move)
        row, col = move[0], move[1]
        row = self.validate_row(row)
        column = self.validate_column(col)
        self.validate_current_cell_value(row, col)
        return [row, column]

    @staticmethod
    def validate_move_length(move):
        if len(move) != 2:
            raise InvalidMove('Input must be length 2 (row, col)')

    def validate_row(self, row):
        try:
            row = int(row)
        except ValueError:
            raise InvalidMove('First value must be an integer (row index)')

        self.validate_row_index(row)
        return row

    def validate_row_index(self, row):
        max_row_index = self.board_size - 1
        if row > max_row_index:
            raise InvalidMove('First value cannot be larger than {}'
                              .format(max_row_index))

    def validate_column(self, col):
        try:
            return self.columns[col]
        except KeyError:
            raise InvalidMove('2nd Value (column) must be one of these: {}'
                              .format(self.column_labels))

    def validate_current_cell_value(self, row, col):
        curr_value = self.board[row][self.columns[col]]
        if curr_value != self.empty_space:
            raise InvalidMove('{}{} is already filled w/ \'{}\''
                              .format(row, col, curr_value))

    @property
    def column_labels(self):
        return ' '.join(self.columns.keys())

    def make_player_move(self, move):
        row, column = move[0], move[1]
        self.make_move(row, column, self.player_value)

    def make_move(self, row, col, value):
        self.board[row][col] = value
        self.evaluate_board(value)

    def make_computer_move(self):
        for row_index, row in enumerate(self.board):
            try:
                col = row.index('-')
                self.make_move(row_index, col, self.computer_value)
                break
            except ValueError:
                continue

    def evaluate_board(self, value):
        self.check_for_winner(value)
        self.check_for_tie()

    def check_for_winner(self, player):
        # Diagonals
        if self.board[0][0] == self.board[1][1] == self.board[2][2] == player \
                or self.board[2][0] == self.board[1][1] == self.board[0][2] == player:
            raise WinException('{} has won!'.format(player))

        # Horizontal or Vertical
        for x in range(3):
            if self.board[x][0] == self.board[x][1] == self.board[x][2] == player or \
                    self.board[0][x] == self.board[1][x] == self.board[2][x] == player:
                raise WinException('{} has won!'.format(player))

    def check_for_tie(self):
        if not any(self.empty_space in sublist for sublist in self.board):
            raise TiedException('It\'s a tie!')

    def reset(self):
        self.board = self.make_board(self.board_size)


def main():
    board_size = 3
    print('Tic Tac Toe!')
    game = TicTacToe(board_size)
    game.print_board()
    while True:
        move = input('Select a cell to play your move (You are X): ')
        try:
            move = game.validate_move(move)
        except InvalidMove as e:
            print(e)
            continue

        try:
            game.make_player_move(move)
            game.make_computer_move()
            game.print_board()
        except (WinException, TiedException) as e:
            print(e)
            game.print_board()
            play_again = input('Would you like to play again?: (y/n) ')
            if play_again == 'y':
                game.reset()
                game.print_board()
                continue
            else:
                sys.exit()


if __name__ == '__main__':
    main()
